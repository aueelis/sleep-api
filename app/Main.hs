{-# LANGUAGE OverloadedStrings #-}
import Web.Scotty
import Control.Concurrent
import           Control.Monad.IO.Unlift        ( liftIO )


main = scotty 3000 $
    get "/:time" $ do
        time <- param "time"
        liftIO . threadDelay $ time * 1000000
        raw "OK"
